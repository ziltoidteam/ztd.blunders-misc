var utils = {};

(function(module) {
    String.prototype.format = function() {
        var str = this;
        for (var i = 0; i < arguments.length; i++) {
            var reg = new RegExp("\\{" + i + "\\}", "gm");
            str = str.replace(reg, arguments[i]);
        }
        return str;
    };

    Number.prototype.pad = function(size) {
        var result = this + "";
        while (result.length < size) result = "0" + result;
        return result;
    };

    if (!Array.prototype.map)
    {
       Array.prototype.map = function(fun /*, thisp*/)
       {
          var len = this.length;

          if (typeof fun != "function")
          throw new TypeError();

          var res = new Array(len);
          var thisp = arguments[1];

          for (var i = 0; i < len; i++)
          {
             if (i in this)
             res[i] = fun.call(thisp, this[i], i, this);
          }
          return res;
       };
    }

    Array.prototype.mapIndex = function(index, f, args) {
        return this.map(function(e) {
            var result = e;
            result[index] = f(result[index], args);

            return result;
        });
    };

    Array.prototype.extract = function(index) {
        return this.map(function (e) {
            return e[index];
        });
    };

    if (!Array.prototype.filter)
    {
       Array.prototype.filter = function(fun /*, thisp*/)
       {
          var len = this.length;
          if (typeof fun != "function")
          throw new TypeError();

          var res = new Array();
          var thisp = arguments[1];
          for (var i = 0; i < len; i++)
          {
             if (i in this)
             {
                var val = this[i]; // in case fun mutates this
                if (fun.call(thisp, val, i, this))
                res.push(val);
             }
          }
          return res;
       };
    }

    if (!Array.prototype.equals) {
            Array.prototype.equals = function (array) {
            // if the other array is a falsy value, return
            if (!array)
                return false;

            // compare lengths - can save a lot of time
            if (this.length != array.length)
                return false;

            for (var i = 0, l=this.length; i < l; i++) {
                // Check if we have nested arrays
                if (this[i] instanceof Array && array[i] instanceof Array) {
                    // recurse into the nested arrays
                    if (!this[i].equals(array[i]))
                        return false;
                }
                else if (this[i] != array[i]) {
                    // Warning - two different object instances will never be equal: {x:20} != {x:20}
                    return false;
                }
            }
            return true;
        }
    }

    Array.prototype.shiftWhile = function(p, aargs) {
        var args = aargs || [];

        var result = [];
        while (this.length > 0) {
            if (!p.apply(null, [this[0]].concat(args))) break;

            result.push(this.shift());
        }

        return result;
    };

    Array.prototype.shiftGroup = function(size) {
        var result = [];
        for (var i = 0; i < size; ++i) {
            result.push(this.shift());
        }

        return result;
    };

    Array.prototype.destructiveChunk = function(groupsize){
        var sets = [], chunks, i = 0;
        chunks = this.length / groupsize;

        while(i < chunks){
            sets[i] = this.splice(0,groupsize);
        i++;
        }

        return sets;
    };

    Array.prototype.chunk = function (groupsize) {
        var sets = [];
        var chunks = this.length / groupsize;

        for (var i = 0, j = 0; i < chunks; i++, j += groupsize) {
          sets[i] = this.slice(j, j + groupsize);
        }

        return sets;
    };

    Array.prototype.maximum = function(comp) {
        var maximum = null;
        this.forEach(function(element){
            if(maximum == null) {
                maximum = element
                return
            }
            if(comp(maximum, element)) {
                maximum = element
            }
        })

        return maximum
    }


    module.injectOnSuccess = function(args, callback) {
      var onSuccesSaved = args.onSuccess
      args.onSuccess = function override(result) {
        onSuccesSaved(result)
        callback(result)
      }
    }

    module.injectOnFail = function(args, callback) {
      var onFailSaved = args.onFail
      args.onFail = function override(result) {
        onFailSaved(result)
        callback(result)
      }
    }

    module.inject = function(args, callback) {
      module.injectOnSuccess(args, callback)
      module.injectOnFail(args, callback)
    }

    module.fixDate = function(rawDate) {
        return new Date(rawDate);
    };

    module.timer = function(interval, callback) {
        if (!callback()) return;

        setTimeout(function() {
            if (!callback()) return;
            module.timer(interval, callback);
        }, interval);
    };

    module.counter = function(interval, tickCallback) {
        var that = {
            startTime: null,
            enabled: false,

            tick: tickCallback,

            total: function () {
                var secondsFromStart = (new Date() - that.startTime) / 1000;
                return Math.round(secondsFromStart);
            },

            start: function () {
                that.startTime = new Date();
                that.enabled = true;

                module.timer(interval, function () {
                    if (!that.enabled) return false;

                    that.tick();

                    return true;
                });
            },

            stop: function () {
                that.enabled = false;
            }
        };

        return that;
    };

    module.ensure = function(interval, timeLimit, repeatCondition, onSuccess, onFail ) {
        if(timeLimit <= 0) {
          onFail()
          return
        }

        if (repeatCondition()) {
          onSuccess()
          return
        }

        setTimeout(function() {
            if (repeatCondition()) {
              onSuccess()
              return
            }
            module.ensure(interval, timeLimit - interval, repeatCondition, onSuccess, onFail);
        }, interval);
    };

    module.delay = function(interval, func) {
        setTimeout(function() {
            func()
        }, interval);
    }

    module.escapeHtml = function(text) {
        return text
            .replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/"/g, '$quot;')
            .replace(/'/g, '&#039;')
            .replace(/\n/g, '<br/>');
    };

    module.timePrettyFormat = function(seconds) {
        var mins = Math.floor(seconds / 60);
        var secs = Math.floor(seconds % 60);

        var spentTimeText = mins + ':' + secs.pad(2);

        return spentTimeText;
    };

    module.dateNiceFormat = function(dateNice) {
        var result = (new Date(dateNice)).toLocaleString("en-US", {
          year: 'numeric', // https://learn.javascript.ru/datetime
          month: 'long',
          day: 'numeric'
        })

        return result
    }

    module.hash = function(str){
      var hash = 0;
      if (str.length == 0) return hash;
      for (var i = 0; i < str.length; i++) {
          var char = str.charCodeAt(i);
          hash = ((hash<<5)-hash)+char;
          hash = hash & hash; // Convert to 32bit integer
      }
      return Math.abs(hash);
    }

    module.normalizeTicks = function(ticks, tickSize) {
        var delta = 0;

        for (var i = 1; i < ticks.length; ++i) {
            delta = ticks[i] - ticks[i-1];

            if (delta > tickSize) {
                ticks.splice(i, 0, ticks[i-1] + tickSize);
            }
        }

        ticks.unshift(ticks[0] - tickSize);
        ticks.push(ticks[ticks.length - 1] + tickSize);
    };

    module.generateTooFewDataMessage = function(message) {
        return '<div style="width: 100%; line-height: 300px; font-size: 200%; color: #CCC;' +
            ' text-align: center;">{0}</div>'.format(message);
    };

    module.insertTooFewDataMessage = function(id, message) {
        $('#{0}'.format(id)).html(module.generateTooFewDataMessage(message));
    };
})(utils);
